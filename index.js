/**
 * Module dependencies
 */
const Resource = require('deployd/lib/resource');
const util = require('util');
const path = require('path');
const debug = require('debug')('vid-dpd-sendmail');
const fs = require('fs');
const nodemailer = require('nodemailer');
const smtp = require('nodemailer-smtp-transport');
const env = process.server.options && process.server.options.env || null;
const appDir = '/../..';
const publicDir = '/../../public';

/**
 * Module setup.
 */
function Sendmail (options) {
  Resource.apply(this, arguments);
  this.config = {
    host: this.config.host,
    port: this.config.port,
    username: this.config.username,
    password: this.config.password,
    fromAddress: this.config.fromAddress,
    replyTo: this.config.replyTo,
    subject: this.config.subject,
    attachmentfilename: this.config.attachmentfilename,
    plainmail: this.config.plainmail,
    htmlmail: this.config.htmlmail,
    directory: this.config.directory
  };

  this.transport = nodemailer.createTransport({
    host: this.config.host, port: this.config.port, secure: false, ignoreTLS: true, tls: {
      rejectUnauthorized: false
    }, auth: {
      type: 'login', user: this.config.username, pass: this.config.password
    }
  }, {
    // default message fields
    from: this.config.fromAddress, replyTo: this.config.replyTo, headers: {
      'Return-Path': this.config.replyTo
    }
  });
}

util.inherits(Sendmail, Resource);

Sendmail.label = 'Sendmail';
Sendmail.events = ['get'];
Sendmail.prototype.clientGeneration = true;
Sendmail.basicDashboard = {
  settings: [{
    name: 'host', type: 'text', description: 'Host name of your SMTP provider. Defaults to \'localhost\'.'
  }, {
    name: 'port', type: 'numeric', description: 'Port number of your SMTP provider. Defaults to 25'
  }, {
    name: 'username', type: 'text', description: 'SMTP username.'
  }, {
    name: 'password', type: 'text', description: 'SMTP password.'
  }, {
    name: 'fromAddress', type: 'text', description: '\'from\' address'
  }, {
    name: 'replyTo', type: 'text', description: '\'replyTo\' address'
  }, {
    name: 'subject', type: 'text', description: 'Subject for the Email'
  }, {
    name: 'attachmentfilename', type: 'text', description: 'Filename of the Attachment'
  }, {
    name: 'plainmail', type: 'text', description: 'Plaintext Template text for the Email'
  }, {
    name: 'htmlmail', type: 'text', description: 'HTML Template text for the Email'
  }, {
    name: 'directory', type: 'text', description: 'Directory where attachments are stored'
  }]
};

/**
 * Module methods
 */
Sendmail.prototype.handle = function (ctx, next) {
  if (ctx.req && ctx.req.method !== 'GET') return next();

  let req = ctx.req, self = this, domain = {
    url: ctx.url, query: ctx.query, body: ctx.body
  };

  let options = req.query || {};
  let attachment = options.attachment;
  let emailTo = options.email;

  let errors = {};
  if (!options.attachment) {
    errors.attachment = '\'attachment\' is required';
  }
  if (!options.email) {
    errors.email = '\'email\' is required';
  }
  if (Object.keys(errors).length) {
    return ctx.done({statusCode: 400, errors: errors});
  }

  let that = this;

  let filePath = path.join(__dirname, publicDir, this.config.directory, attachment);
  let base64 = base64_encode(filePath);

  let message = {
    from: this.config.fromAddress, replyTo: this.config.replyTo, headers: {
      'Return-Path': this.config.replyTo
    }, to: emailTo, subject: this.config.subject, text: this.config.plainmail, html: this.config.htmlmail, attachments: []
  };

  console.log('prepared message', message);

  let filetype = path.extname(this.config.attachmentfilename).toLowerCase();

  switch (filetype) {
    case '.jpg':
    case '.jpeg':
      message.attachments = [{
        filename: this.config.attachmentfilename,
        content: new Buffer(base64, 'base64'),
        contentType: 'image/jpeg',
        contentDisposition: 'attachment',
        headers: {
          'Content-type': 'image/jpeg'
        }
      }];
      break;
    case '.mp4':
      message.attachments = [{
        filename: this.config.attachmentfilename,
        content: new Buffer(base64, 'base64'),
        contentType: 'video/mp4',
        contentDisposition: 'attachment',
        headers: {
          'Content-type': 'video/mp4'
        }
      }];
      break;
  }

  that.transport.sendMail(message, function (err, info) {
    if (err) {
      return ctx.done(err);
    }
    ctx.done(null, {message: info.response});
  });
};

// function to encode file data to base64 encoded string
function base64_encode (file) {
  // read binary data
  let attachmentBinary = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(attachmentBinary).toString('base64');
}

/**
 * Module export
 */
module.exports = Sendmail;
